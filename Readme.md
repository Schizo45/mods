Great White North A21 with TONS of modlets and Botman

The following modlets are included. If you like one or more of those modlets a lot, please say thank you to the creators.

Mods Included:

Lots of Vehicles - dk710
Compact Dew Collector - ligmaballzXD
Railgun - Snufkin, with update by arramus
Server Side Weapons - nufkin, oakraven, arramus, and community members
Server Side Zombies - arramus and oakraven (based on, and incorporating Snufkin Zombies features and concept)
Add Acid - Pahbi
Always get a side when harvesting - Pahbi
Vehicle Respawner - Anabella
Bigger Bookcases - BagelShoes
Bdubs Vehicles - bdubyah
Hay Blocks No Fall Damage - Claymore
Cop Spit Removal - JaxTeller718
Craft Tier 6 - Pahbi
Craft Hardened Chests - BoxTurtle Games
Craft Empty Jars - 4XJust
Craft from Containers - aedenthorn
Auto Miner - DEVGANG

From Donovan:
Better Cement
Better Traps
Mega Stacks

Ghillie Suit as a mod - Gazz
Beautiful Bases - GNS_Custom
Hydroponic Farming - WimpingEgo
Animals Overhaul - Iceburg71
15 slot Toolbelt - KhaineGB
Zombie Fall Damage Increase - KhaineGB
Craft Vanilla Doors - KeuleAufKopf
Less Rain - Pahbi
Magbow - RussianDood
More Containers - Éric Beaudon
Nicer Birds Nests - JaxTeller718
Pin Recipes - ocbMaurice
Palladium Glass - Daddy Jack
PEP POIExpansion Pack - Khaine, Krunch, Lemon
Pick Up Props - MajDash
Working Decor - r4pt
Repair With - Djtriplexmr
Grass Cutter - RussianDood
Simple UI - RevenantWit
Server Side Starter Items - Cornl3read
Sidewalk 13m Shapes - Sunderbraze
Snow to Water - Pahbi
Snufkin Paintings - Snufkin
Soccer Pole Flags - nobody
Legacy Distant Terrain - Spherii
Skip Tutorial Quests - Spherii
Working Stuff - Saminal
Better Block Upgrades - TantraMan
Burning Blocks - TantraMan
TE Zombies - TormentedEmu
Valmars Disable Falling Trees - Valmar
War3zuk Paintings and Pictures - War3zuk

Oakraven Things - oakraven, with support from arramus:
Bee Hives
Chicken Coops
Cooking Stations
Fish Farm
Mono Bikes
Power Things
Rock Drill

Art Studio - Ztensity
Industrial Resources - Ztensity
Unnecessarily Beautiful But Immersive - Ztensity
Reinforced Chainlink Fence - Jakmeister999
More Melee Weapons - josefpatch
Tactical Weapons - josefpatch